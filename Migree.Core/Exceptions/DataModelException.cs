﻿namespace Migree.Core.Exceptions
{
    public class DataModelException : MigreeException
    {
        public DataModelException(string message)
            : base(message)
        { }
    }
}
