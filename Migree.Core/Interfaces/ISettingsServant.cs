﻿namespace Migree.Core.Interfaces
{
    public interface ISettingsServant
    {
        string StorageConnectionString { get; }
    }
}
